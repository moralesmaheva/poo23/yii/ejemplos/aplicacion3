<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Depart $model */

$this->title = 'Actualizar Departamento: ' . $model->dept_no;
$this->params['breadcrumbs'][] = ['label' => 'Departs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dept_no, 'url' => ['view', 'dept_no' => $model->dept_no]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="depart-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
