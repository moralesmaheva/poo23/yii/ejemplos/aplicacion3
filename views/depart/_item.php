<div class="card">
    <div class="card-body">
        <h5 class="card-title">Departamento <?= $model->dept_no ?></h5>
    </div>
    <ul class="list-group list-group-flush">
        <li class="list-group-item"><?= $model->getAttributeLabel('dnombre') ?>: <?= $model->dnombre ?></li>
        <li class="list-group-item"><?= $model->getAttributeLabel('loc') ?>: <?= $model->loc ?></li>
    </ul>
</div>