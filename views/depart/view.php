<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Depart $model */

$this->title = $model->dept_no;
$this->params['breadcrumbs'][] = ['label' => 'Departs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="depart-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Editar', ['update', 'dept_no' => $model->dept_no], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'dept_no' => $model->dept_no], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Esta seguro de que quiere eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dept_no',
            'dnombre',
            'loc',
        ],
    ]) ?>

</div>
