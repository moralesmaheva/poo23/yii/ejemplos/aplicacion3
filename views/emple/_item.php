<div class="card">
        <div class="card-body">
            <h5 class="card-title">Empleado numero <?= $model->emp_no ?></h5>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item"><?= $model->getAttributeLabel('apellido')?>: <?= $model->apellido ?></li>
            <li class="list-group-item"><?= $model->getAttributeLabel('oficio')?>: <?= $model->oficio ?></li>
            <li class="list-group-item"><?= $model->getAttributeLabel('dir')?>: <?= $model->dir ?></li>
            <li class="list-group-item"><?= $model->getAttributeLabel('fecha_alt')?>: <?= $model->fecha_alt ?></li>
            <li class="list-group-item"><?= $model->getAttributeLabel('salario')?>: <?= $model->salario ?></li>
            <li class="list-group-item"><?= $model->getAttributeLabel('comision')?>: <?= $model->comision ?></li>
            <li class="list-group-item"><?= $model->getAttributeLabel('dept_no')?>: <?= $model->dept_no ?></li>
            <li class="list-group-item"><?= $model->getAttributeLabel('deptNo.dnombre')?>: <?= $model->deptNo->dnombre ?></li>
        </ul>
    </div>
